package cyappstore.flickrchallenge.data.service;

import cyappstore.flickrchallenge.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by colin.
 */

public class FlickrFactory {

    public static FlickrService create() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(BuildConfig.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(getLoggerClient())
                .build();
        return retrofit.create(FlickrService.class);
    }

    /**
     * Method to build logger client for RestAdapter
     * @return
     */
    private static OkHttpClient getLoggerClient(){

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();

        if (BuildConfig.DEBUG){
//            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        }

        return new OkHttpClient.Builder().addInterceptor(loggingInterceptor).build();
    }
}
