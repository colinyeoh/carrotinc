package cyappstore.flickrchallenge.data.service;

import cyappstore.flickrchallenge.data.model.GetPhotosResponse;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by colin.
 */

public interface FlickrService {

    // We can obtain this from server in the future.
    String KEY = "ece2e8dfc3b0dcb883577e3d6bad6290";

    // We will use a fix key for now.
    String METHOD = "flickr.photos.search";
    String EXTRAS = "url_s";
    String FORMAT = "json";
    int JSON_CALLBACK_FLAG = 1;

    @GET(".")
    Observable<GetPhotosResponse> searchImages(
            @Query("method") String method,
            @Query("api_key") String key,
            @Query("text") String queryKey,
            @Query("extras") String extras,
            @Query("format") String format,
            @Query("nojsoncallback") int noJsonCallbackFlag,
            @Query("page") int page);
}
