package cyappstore.flickrchallenge.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import cyappstore.flickrchallenge.R;
import cyappstore.flickrchallenge.data.model.GalleryItem;
import cyappstore.flickrchallenge.Presenter.SearchScreenPresenter;
import cyappstore.flickrchallenge.Presenter.SearchScreenImpl;

/**
 * Created by ColinYeoh
 */

public class SearchScreenFragment extends Fragment implements SearchScreenImpl {

    private SearchScreenPresenter screenViewModel;
    private String PHOTO_KEY = "photo_key";

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_main_fragment, container, false);

        this.screenViewModel = new SearchScreenPresenter();

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.screenViewModel.setupView(view, this);

    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showFullScreenImage(GalleryItem photo) {
        //TODO:: Can implement launch full screen image and make model parcelable/serialable
//        Intent intent = new Intent(getActivity(),FullscreenImage.class);
//        intent.putExtra(PHOTO_KEY, photo);
//        startActivity(intent);

    }
}
