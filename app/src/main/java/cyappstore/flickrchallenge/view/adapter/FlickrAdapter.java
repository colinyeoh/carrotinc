package cyappstore.flickrchallenge.view.adapter;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.facebook.drawee.view.SimpleDraweeView;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cyappstore.flickrchallenge.R;
import cyappstore.flickrchallenge.data.model.GalleryItem;
import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by ColinYeoh
 */

public class FlickrAdapter extends RecyclerView.Adapter<FlickrAdapter.ViewHolder> {

    private List<GalleryItem> dataSet;
    private final PublishSubject<GalleryItem> onClickSubject = PublishSubject.create();

    public FlickrAdapter(List<GalleryItem> dataSet) {
        this.dataSet = dataSet;
    }

    @NonNull
    @Override
    public FlickrAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_cell, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull FlickrAdapter.ViewHolder holder, int position) {
        final GalleryItem photo = this.dataSet.get(position);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSubject.onNext(photo);
            }
        });

        holder.imageTitle.setText(photo.getTitle() != null ?
                photo.getTitle() :
                holder.itemView.getContext().getText(R.string.no_title));

        if (photo.getUrl() != null){
            holder.imageView.setImageURI(Uri.parse(photo.getUrl()));
        }
    }

    @Override
    public int getItemCount() {
        return this.dataSet.size();
    }

    public Observable<GalleryItem> getPositionClicks(){
        return onClickSubject.asObservable();
    }

    public void updateDataSet(List<GalleryItem> dataSet){
        this.dataSet = dataSet;
        notifyDataSetChanged();
    }

    public void addIntoDataSet(List<GalleryItem> dataSet) {
        this.dataSet.addAll(dataSet);
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_title)
        TextView imageTitle;
        @BindView(R.id.my_image_view)
        SimpleDraweeView imageView;

        //TODO:: We can include more parameters from the response here if needed

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }
    }

}
