package cyappstore.flickrchallenge.Presenter;

import cyappstore.flickrchallenge.data.model.GalleryItem;

/**
 * Created by ColinYeoh
 */

public interface SearchScreenImpl {

    // TODO:: if action needed from the main view, we can add callback method here

    void showToastMessage(String message);

    void showFullScreenImage(GalleryItem photo);
}
