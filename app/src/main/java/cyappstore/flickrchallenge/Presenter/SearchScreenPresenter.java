package cyappstore.flickrchallenge.Presenter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import cyappstore.flickrchallenge.BaseApplication;
import cyappstore.flickrchallenge.R;
import cyappstore.flickrchallenge.data.model.GalleryItem;
import cyappstore.flickrchallenge.data.model.GetPhotosResponse;
import cyappstore.flickrchallenge.data.service.FlickrService;
import cyappstore.flickrchallenge.view.adapter.FlickrAdapter;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by ColinYeoh
 */

public class SearchScreenPresenter {

    private Context context;
    private SearchScreenImpl contract;

    private FlickrAdapter imageAdapter;
    private List<GalleryItem> dataSet;
    private FlickrService restService;
    private int page = 1;

    @BindView(R.id.autoCompleteTextView) AutoCompleteTextView searchText;
    @BindView(R.id.resultList) RecyclerView resultRecyclerView;

    public void setupView(View view, SearchScreenImpl contract) {
        this.context = view.getContext();
        this.contract = contract;

        ButterKnife.bind(this, view);

        this.setupRestCall();

        this.setupRecyclerView();
    }

    private void setupRecyclerView(){

        this.resultRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.context);
        this.resultRecyclerView.setLayoutManager(mLayoutManager);

        this.dataSet = new ArrayList<>();
        imageAdapter = new FlickrAdapter(this.dataSet);
        this.resultRecyclerView.setAdapter(imageAdapter);

        // need to handle pagination when recycleview is scrolled to the end of list
        this.resultRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                // direction integers: -1 for up, 1 for down
                if (!recyclerView.canScrollVertically(1)) {
                    nextPage();
                }
            }
        });

        // Setup item click observer
        imageAdapter.getPositionClicks()
                .subscribe(new Action1<GalleryItem>() {
                    @Override
                    public void call(GalleryItem galleryItem) {
                        onItemClicked(galleryItem);
                    }
                });
    }

    private void onItemClicked(GalleryItem galleryItem){
        //TODO:: determine what is the next action
    }

    private void resetPage(){
        page = 1;
    }

    private void incrementPage(){
        page++;
    }

    private void nextPage(){

        incrementPage();

        if (searchText != null)
            onAutoTextChanged(searchText.getText().toString(), 0,0, 1);
    }

    private void setupRestCall(){
        restService = BaseApplication.create(this.context).getRestService();
    }

    @OnTextChanged(value = R.id.autoCompleteTextView, callback = OnTextChanged.Callback.TEXT_CHANGED)
    public void onAutoTextChanged(CharSequence text, int start, int before, int count){

        // count < 1 if input is reduced, we reset the list
        if (count <= 0){
            resetPage();
            resultRecyclerView.scrollToPosition(0);
        }

        // Set minimum characters needed to search
        if (text.length() > 2){
            this.search(text.toString(), page);
        }
    }

    private void search(String value, final int page){

        this.restService.searchImages(
                FlickrService.METHOD,
                FlickrService.KEY,
                value,
                FlickrService.EXTRAS,
                FlickrService.FORMAT,
                FlickrService.JSON_CALLBACK_FLAG,
                page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<GetPhotosResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        contract.showToastMessage(e.getMessage());
                    }

                    @Override
                    public void onNext(GetPhotosResponse flickrResponse) {
                        if (flickrResponse.getPhotoMeta() != null
                                && flickrResponse.getPhotoMeta().getGalleryItems() != null){

                            // If current page is more than 1 then we will append into list instead
                            if (page > 1)
                                imageAdapter.addIntoDataSet(flickrResponse.getPhotoMeta().getGalleryItems());

                            else
                                imageAdapter.updateDataSet(flickrResponse.getPhotoMeta().getGalleryItems());

                        } else {
                            contract.showToastMessage(context.getString(R.string.no_photo));
                        }
                    }
                });
    }
}
