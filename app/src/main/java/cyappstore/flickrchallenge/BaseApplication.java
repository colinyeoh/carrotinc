package cyappstore.flickrchallenge;

import android.app.Application;
import android.content.Context;

import com.facebook.drawee.backends.pipeline.Fresco;

import cyappstore.flickrchallenge.data.service.FlickrFactory;
import cyappstore.flickrchallenge.data.service.FlickrService;

/**
 * Created by ColinYeoh
 */

public class BaseApplication extends Application {

    private FlickrService restService;

    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
    }

    private static BaseApplication get(Context context) {
        return (BaseApplication) context.getApplicationContext();
    }

    public static BaseApplication create(Context context) {
        return BaseApplication.get(context);
    }

    public FlickrService getRestService() {
        if (restService == null) restService = FlickrFactory.create();

        return restService;
    }
}
